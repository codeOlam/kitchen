![Django Logo](https://cdn0.froala.com/assets/editor/pages/B/frameworks/large/django-54a8740593da0031831666b94c418bc9.jpg)


## SreenShots


![Home_page](screenshots/Kitchen_Menu_List.png?raw=true "homepage")




![Add_Menu](screenshots/Kitchen_Add_Menu.png?raw=true "add")




![Add_Menu](screenshots/Kitchen_Update_Menu.png?raw=true "update")



![Login_page](screenshots/Kitche_Login.png?raw=true "Login")




![SignIn_page](screenshots/Kitchen_Sign_Up.png?raw=true "Signup")



![User_dash Board](screenshots/Kitche_Profile.png?raw=true "Dash")


## About
Django is a web-framework written in Python and runs the backend for many of the internet's most popular websites. This app is built ontop of Django 2.2.3. Just a basic intoduction to Django's cool feature, teaching you how to implement; Views, data base models, urls routing etc.  


This app features the following:

-- CRUD in django:

Here you CREATE (add new menu), READ (view menu details), UPDATE(change Menu information) and DELETE (remove menu from list)


-- It also introduces basic Django authentication

I made use of Django CustomUserModel; this is advised when starting a project incase you need to extend the apllications authentication during development or in production. so the basic features like, LOGIN, LOGOUT, SIGNUP, VIEW USER PROFILE and UPDATE profile was implemented in the project.


## Technology and Requirements
1. Django 2.2.3
2. Python3
3. postgresql(using pyscopg2 as the adapter for python3)
4. Boostrap 4.3.x (for front end)

## Installations
1. [installing Python3](https://www.python.org/downloads/)
2. [installing Django 2.2](https://docs.djangoproject.com/en/2.2/topics/install/)
3. [installing Virtualenv](https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/)
4. [installing and setting up Postgres](http://www.techken.in/how-to/install-postgresql-10-windows-10-linux/)
5. installing requirements from requirements.txt. After activating vitualenv run:

`(venv)path/to/kitchen/src$ pip install -r requirements.txt
`

6. [psycopg2](http://initd.org/psycopg/docs/install.html)


## Run App
1. make sure your virtualenv is ativated.

`
(venv)path/to/kitchen/src$
`

2. make sure you are in the same directory where manage.py is then run

`(venv)path/to/kitchen/src$ python manage.py runserver
`

3. go to your web browser and enter 127.0.0.1:8000 

## Recommendations

Few recommendations are to experiment and build on top of this app, you can fork it. or use the knowledge to solve your own challenges. you can add more beauty to the front-end, and other cool features like; 

1. booking reservations, 
2. Associate Newly added menu to specific users
3. create permission for only registered users to able to CREATE and UPDATE menu list
4. maps and so on...

## Resources
1. [Django 2.2.3 Doc](https://docs.djangoproject.com/en/2.2/)
2. [Coding for Entrepreneurs](http://joincfe.com/projects/).
3. [Cfe YouTube channel](http://joincfe.com/youtube)
4. [William Vincent's blog](https://wsvincent.com/django-custom-user-model-tutorial/)

### Other Resources
1. [Custom User Model](https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project) 
2. [boostrap doc](https://getbootstrap.com/docs/4.3/layout/grid/)
3. [bootsrap cdn](https://www.bootstrapcdn.com/)
