from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import KitchenUser

class KitchenUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model       = KitchenUser
        fields      = ( 'username', 
                        'email'
                        )

class KitchenUserChangeForm(UserChangeForm):

    class Meta(UserChangeForm):
        model       = KitchenUser
        fields      = ( 'username',
                        'email',
                        )
