from django.db import models
from django.contrib.auth.models import AbstractUser, User
from django.urls import reverse
from django.shortcuts import redirect
# Create your models here.

class KitchenUser(AbstractUser):
    pass

    #def __str__(self):
        #pass
        #return self.name

    def get_absolute_url(self):
    	return redirect("profile:user_profile", args=[self.pk])