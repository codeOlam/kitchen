from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views

from django.views.generic import TemplateView

from .views import (
        ProfileDetails,
        UpdateProfileForm,
        )

urlpatterns = [
    path('user/<int:pk>/update_profile/', UpdateProfileForm.as_view(), name='update_profile'),
    path('user/<int:pk>/', ProfileDetails.as_view(), name='user_profile'),

]
