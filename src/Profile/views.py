from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import CreateView, UpdateView, DeleteView 
from django.views.generic import DetailView, TemplateView
from django.urls import reverse_lazy, reverse

from .models import KitchenUser
from .forms import KitchenUserCreationForm
# Create your views here

class CreateProfile(CreateView):
    form_class      = KitchenUserCreationForm
    template_name   = 'Profile/registration/signup.html'
    success_url     = reverse_lazy('menu_list')


class ProfileDetails(TemplateView):
    template_name   = 'Profile/dash.html'


class UpdateProfileForm(UpdateView):
    model           = KitchenUser
    fields          = ['username', 'first_name', 'last_name', 'email']
    template_name   = 'Profile/update_profile.html'
