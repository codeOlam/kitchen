from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model

from .models import KitchenUser
from .forms import KitchenUserCreationForm, KitchenUserChangeForm
# Register your models here.


class KitchenUserAdmin(UserAdmin):
    add_form        = KitchenUserCreationForm
    form            = KitchenUserChangeForm
    model           = KitchenUser
    list_display    = [ 'username',
                        'email'
                        ]


admin.site.register(KitchenUser, KitchenUserAdmin)
