"""kitchen URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views

from django.views.generic import TemplateView

from restaurants.views import (
        ListMenu,
        MenuDetails,
        CreateMenuForm,
        UpdateMenuForm,
        DeleteMenuForm
        )
from Profile.views import CreateProfile


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', ListMenu.as_view(), name='menu_list'),
    path('restaurant/create_menu/', CreateMenuForm.as_view()),
    path('restaurant/<slug:slug>/update_menu/', UpdateMenuForm.as_view(), name='update_menu'),
    path('restaurant/<slug:slug>/delete_menu/', DeleteMenuForm.as_view(), name='delete_menu'),
    path('restaurant/<slug:slug>/', MenuDetails.as_view(), name='menu_detail'),
    path('about/', TemplateView.as_view(template_name="about.html")),
    path('contact/', TemplateView.as_view(template_name="contact.html")),
    path('signup/', CreateProfile.as_view(), name='signup'),
    path('login/', auth_views.LoginView.as_view(template_name='Profile/registration/login.html')),
    path('profile/user/<int:pk>/logout', auth_views.LogoutView.as_view()),
    path('', include('django.contrib.auth.urls')),
    path('profile/', include(('Profile.urls', 'profile'), namespace='profile')),

]
