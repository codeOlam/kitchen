from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView, CreateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.db.models import Q
from django.http import HttpResponse, HttpRequest
from django.urls import reverse_lazy, reverse


from .models import Menu
from .forms import MenuCreateForm
# Create your views here

class ListMenu(ListView):

    def get_queryset(self):
        slug = self.kwargs.get('slug')

        if slug:
            queryset = Menu.objects.filter(
                    Q(food_name__iexact=slug)|
                    Q(Location__icontains=slug)
                    )
        else:
            queryset = Menu.objects.all()
        return queryset

class MenuDetails(DetailView):
    queryset = Menu.objects.all()

    def get_absolute_url(self):
        return redirect('menu_detail', slug=slug)

class CreateMenuForm(CreateView):
    template_name = 'restaurants/create_menu_form.html'
    model = Menu
    fields = ['owner', 'food_name', 'meal', 'is_available', 'is_Finished', 'Location']



class UpdateMenuForm(UpdateView):
    model = Menu
    fields = ['owner', 'food_name', 'meal', 'is_available', 'is_Finished', 'Location']
    template_name = 'restaurants/update_menu_form.html'


    def get_absolute_url(self):
        return reverse('update_menu', kwargs={'slug': self.slug})


class DeleteMenuForm(DeleteView):
    model = Menu
    template_name = 'restaurants/menu_confirm_delete.html'
    success_url = reverse_lazy('menu_list')

    def get_absolute_url(self):
        return reverse('delete_menu', kwargs={'slug': self.slug})
