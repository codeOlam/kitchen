# Generated by Django 2.2.3 on 2019-08-22 09:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('food_name', models.CharField(max_length=120)),
                ('slug', models.SlugField(blank=True, null=True, unique=True)),
                ('meal', models.CharField(choices=[('Breakfast', 'breakfast'), ('Lunch', 'lunch'), ('Dinner', 'dinner')], help_text='Select Meal', max_length=10, null=True)),
                ('is_available', models.BooleanField()),
                ('is_Finished', models.BooleanField()),
                ('time_served', models.DateTimeField(auto_now=True, null=True)),
                ('updated', models.DateTimeField(auto_now_add=True, null=True)),
                ('Location', models.CharField(max_length=120)),
                ('photo', models.ImageField(blank=True, max_length=120, upload_to=None)),
                ('owner', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
