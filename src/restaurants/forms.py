from django.forms import ModelForm

from .models import Menu

class MenuCreateForm(ModelForm):
    class Meta:
        model = Menu
        fields = ['food_name', 'meal', 'Location']
