from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.urls import reverse

from .utils import unique_slug_generator

#Create your models here.

User = settings.AUTH_USER_MODEL

meal_list = ( ('Breakfast', 'breakfast'),
                ('Lunch', 'lunch'),
                ('Dinner', 'dinner')
            )
class Menu(models.Model):
    owner               = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    food_name           = models.CharField(max_length=120, blank=False)
    slug                = models.SlugField(unique=True, blank=True, null=True)
    meal                = models.CharField(max_length=10, choices=meal_list, help_text="Select Meal", null=True)
    is_available        = models.BooleanField()
    is_Finished         = models.BooleanField()
    time_served         = models.DateTimeField(auto_now=True, null=True)
    updated             = models.DateTimeField(auto_now_add=True, null=True)
    Location            = models.CharField(max_length=120)
    photo               = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=120, blank=True)
    
    def __str__(self):
        return self.food_name

    @property
    def title(self):
        return self.food_name


    def get_absolute_url(self):
        return reverse('menu_detail', kwargs={'slug': self.slug})




def menu_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(menu_pre_save_receiver, sender=Menu)
